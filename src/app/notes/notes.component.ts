import { Component, OnInit } from '@angular/core';
import { DataService } from '../core/data.service';
import { Note, NoteAdapter } from '../core/note.model';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {

  createForm: FormGroup;
  dataService: DataService;
  notes: Observable<Note[]>;

  constructor(dataService: DataService, private formBuilder: FormBuilder) {
    this.dataService = dataService;
    this.notes = dataService.getAllNotes();
  }

  ngOnInit() {
    this.createForm = this.formBuilder.group({title: ['']});
  }

  newNoteSubmit() {
    this.dataService.createNote(
      new Note(null, this.createForm.get('title').value)
    ).subscribe(
      note => console.log(note)
    );
    // clear input
    this.createForm.setValue({title: ''})
  }

}
