import { Component, OnInit, TemplateRef } from '@angular/core';
import { DataService } from '../core/data.service';

import { ActivatedRoute, Router } from "@angular/router";

import { Note, NoteAdapter } from '../core/note.model';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.css']
})
export class NoteComponent implements OnInit {

  modalRef: BsModalRef;
  submitForm: FormGroup;
  dataService: DataService;
  id: number;
  note = new Observable<Note>();
  localNote = null;

  constructor(
    dataService: DataService,
    private route: ActivatedRoute,
    public router: Router,
    private formBuilder: FormBuilder,
    private bsmodalservice: BsModalService
  ) {
    this.id = parseInt( this.route.snapshot.paramMap.get("id") );
    this.note = dataService.getNoteById( this.id );
    this.dataService = dataService;
  }

  ngOnInit() {
    this.submitForm = this.formBuilder.group({title: ['']});
    this.note.subscribe(
      item => this.submitForm.setValue({title: item.title})
    );
  }

  public deleteNoteClick() {
    console.log("Deleting note id: " + this.id);
    this.dataService.deleteNoteById(this.id).subscribe();

    this.router.navigate(['../'], { relativeTo: this.route });
  }

  public updateNoteSubmit() {
    console.log("Updating note id: " + this.id + " " + this.submitForm.get('title').value);
    this.dataService.updateNoteById(
      this.id,
      new Note(this.id, this.submitForm.get('title').value)
    ).subscribe();

    this.router.navigate(['../'], { relativeTo: this.route });
  }

  openModal(template: TemplateRef<any>) {
      this.modalRef = this.bsmodalservice.show(template);
  }

  confirm(): void {
    this.deleteNoteClick();
    this.modalRef.hide();
  }

  decline(): void {
    this.modalRef.hide();
  }

}
