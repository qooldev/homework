import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Note, NoteAdapter } from './note.model';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private baseUrl = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private adapter: NoteAdapter,
  ) { }

  public getAllNotes(): Observable<Note[]> {
    const url = `${this.baseUrl}/notes`;
    return this.http.get(url).pipe(
      // Adapt each item in the raw data array
        map( (data: any[]) => data.map( item => this.adapter.adapt(item) ) ),
      );
  }

  public getNoteById(id: number): Observable<Note> {
    const url = `${this.baseUrl}/notes/${id}`;
    return this.http.get(url).pipe(
         map( item => this.adapter.adapt(item) ),
      );
  }

  public createNote(note: Note): Observable<Note> {
    const url = `${this.baseUrl}/notes`;
    return this.http.post<Note>(url, note).pipe(
         map( item => this.adapter.adapt(item) ),
      );
  }

  public updateNoteById(id: number, note: Note): Observable<Note> {
    const url = `${this.baseUrl}/notes/${id}`;
    return this.http.put<Note>(url, note).pipe(
         map( item => this.adapter.adapt(item) ),
      );
  }

  public deleteNoteById(id: number): Observable<null> {
    const url = `${this.baseUrl}/notes/${id}`;
    return this.http.delete(url).pipe(
         map(response => null)
      );
  }

}
