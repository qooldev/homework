import { Injectable } from '@angular/core';
import { Adapter } from './adapter';

export class Note {
  constructor(
    public id: number,
    public title: string,
  ) { }
}

@Injectable({
    providedIn: 'root'
})
export class NoteAdapter implements Adapter<Note> {
  adapt(item: any): Note {
    return new Note(
      item.id,
      item.title,
    );
  }
}
