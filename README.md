# Homework

Homework Angular 7 / REST project

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.0.

## Installation

(Tested with npm `3.5.2`, `6.7.0` and nodejs `8.10.0`, `11.9.0`)


`git clone https://qooldev@bitbucket.org/qooldev/homework.git`

Enter project path  
`cd homework`  

Install dependencies  
`npm install .`  

Build locales  
`npm run build-locale`  

Install angular-cli for ng builder  
`npm install -g @angular/cli`  

### Running in production

Build production bundle
`ng build --prod`

and set/reload your webserver (apache, nginx...) to serve contents of `/homework-project-root/homework/dist/homework` directly.

Navigate to `http://homework-public-ip-address/`.

### Running in development mode

_NOTE: The language selector buttons in application will NOT work in development mode (build ahead-of-time). Optionally the app may be run with `--environment=[cs|en]` to switch languages_

Run `ng serve --port 9000 --environment=en` for a dev server.

Navigate to `http://localhost:9000/`.

## Running tests

`ng test`
