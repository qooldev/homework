import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';

import { NotesComponent } from './notes/notes.component';
import { NoteComponent } from './note/note.component';

const routes: Routes = [
  { path: '',  redirectTo: '/notes', pathMatch: 'full' },
  { path: 'notes', component: NotesComponent },
  { path: 'notes/:id', component: NoteComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { enableTracing: true }),
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    ModalModule.forRoot()
  ],
  declarations: [
    NotesComponent,
    NoteComponent
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
